from scapy.all import rdpcap,Raw
import numpy as np
import pandas as pd
import struct
import warnings

warnings.filterwarnings("ignore")


def interpPCAP(filePath):
    scapy_cap = rdpcap(filePath)
    dfList = []
    for packet in scapy_cap:
        try:
            byteData = bytes(packet.getlayer(Raw))
            header = np.frombuffer(byteData[:10], dtype=np.dtype([('type', 'uint8'), ('order', 'uint8'), ('frameID', 'uint32'), ('packetID', 'uint16'),('datalen', 'uint16')]))
            df = pd.DataFrame(np.frombuffer(byteData[10:], dtype='int16').reshape(-1, 4),columns=['distanceInd', 'intensity', 'N_mir', 'N_tx'])
            df['frameID'] = header['frameID'][0]
            df['packetID'] = header['packetID'][0]
            dfList.append(df)
        except TypeError:
            print( 'cannot convert NoneType object to bytes')
            continue
    dfAll = pd.concat(dfList)
    dfAll = dfAll[dfAll['N_tx'] != 0] #clean unsused transmitters
    dfAll = dfAll.reset_index(drop=True)
    return dfAll

#msg type(1B) , packet order(1B), frameID(4B), packetID(2B), datalen (2B) , data 8000

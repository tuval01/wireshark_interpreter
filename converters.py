import pandas as pd
import numpy as np
from pathlib import Path

path = Path(__file__).parent

tx_steering_mapping_filepath = '{}/tx beam steering angle mapping.csv'.format(path)
RxTxTopology_filepath = '{}/TX-RX-DET_topology.csv'.format(path)

VgrooveConvertionDF = pd.read_csv(tx_steering_mapping_filepath)
VgrooveConvertionDF = VgrooveConvertionDF.reset_index()
tx2zenith = VgrooveConvertionDF['tx_elevation']
tx2azimuth = VgrooveConvertionDF['tx_azimuth']

RxTxTopologyDF = pd.read_csv(RxTxTopology_filepath)
RxTxTopologyDF = RxTxTopologyDF.reset_index()
tx2rx = RxTxTopologyDF['N_rx']
tx2detector = RxTxTopologyDF['detector']


mirrorConst = 5.16E-5
distanceInd2distanceConst = (2.998E8*2E-9/2)/4
istanceIndexOffset = 81 #-72

def getElevationSteering(N_tx):
    return tx2zenith[N_tx-1]

def getAzimuthSteering(N_tx,column):
    return tx2azimuth[N_tx-1] + column * mirrorConst

def distanceInd2distance(distanceInd):
    return (distanceInd - istanceIndexOffset) * distanceInd2distanceConst

def getRx(N_tx):
    return tx2rx[N_tx - 1]

def getDetector(N_tx):
    return tx2detector[N_tx - 1]

import PlotlyPlotter
import pyshark_interpreter_native
import PointCloud
import pandas as pd


df = pyshark_interpreter_native.interpPCAP('test_data/saturation at 0 reflector stripes on wall.pcap')
df2 = PointCloud.generatePointCloud(df[:5000])

# PlotlyPlotter.plotPointCloud(df2)

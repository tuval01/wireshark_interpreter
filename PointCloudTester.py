import PlotlyPlotter
import pyshark_interpreter_native
import PointCloud
import pandas as pd
import numpy as np
import importlib
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import scipy.signal as signal
pd.options.plotting.backend = "plotly"

df = pyshark_interpreter_native.interpPCAP(r'D:\plate.pcap')

Nframes = 1
df2 = df[df.frameID<(df.frameID[0]+Nframes)]

importlib.reload(PointCloud)

pointcloud = PointCloud.generatePointCloud(df2)
pointcloud = pointcloud[(pointcloud.intensity>0)]

# PlotlyPlotter.plotPointCloud(pointcloud)
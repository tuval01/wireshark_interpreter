import numpy as np
import pandas as pd
import converters
def generatePointCloud(df):
    # df2 = pd.merge(df2, tx_df, on='row', how='inner')
    df['elevation'] = df['N_tx'].apply(lambda x: converters.getElevationSteering(x))
    df['azimuth'] = df.apply(lambda x: converters.getAzimuthSteering(x['N_tx'], x['N_mir']), axis=1)


    df['N_rx'] = df['N_tx'].apply(lambda x: converters.getRx(x))
    df['detector'] = df['N_tx'].apply(lambda x: converters.getDetector(x))

    df['distance'] = df.distanceInd.apply(lambda x: converters.distanceInd2distance(x))
    df['x'] = df['distance'] * np.cos(df.elevation) * np.sin(df.azimuth)
    df['y'] = df['distance'] * np.cos(df.elevation) * np.cos(df.azimuth)
    df['z'] = df['distance'] * np.sin(df.elevation)
    df = df.reset_index(drop=True)
    return df
    # dfCloud['x'] = dfCloud.distance * np.cos((dfCloud.theta-16.5) * 0.17/31-np.deg2rad(thetaInclination)) * np.cos((dfCloud.phi) * 2.12e-4)
    # dfCloud['y'] = dfCloud.distance * np.cos((dfCloud.theta-16.5) * 0.17/31-np.deg2rad(thetaInclination)) * np.sin((dfCloud.phi) * 2.12e-4)
    # dfCloud['z'] = -dfCloud.distance * np.sin((dfCloud.theta - 16.5) * 0.17 / 31-np.deg2rad(thetaInclination))
    # return dfCloud

import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from plotly.validators.scatter.marker import SymbolValidator
raw_symbols = SymbolValidator().values

def plotPointCloud(dfCloud,x='x',y='y',z='z',color='intensity',symbol='symbol',scene_camera=None,hoverData=["N_mir", "N_tx","distanceInd"]):
    max_range = np.array([dfCloud[x].max() - dfCloud[x].min(), dfCloud[y].max() - dfCloud[y].min(),
                          dfCloud[z].max() - dfCloud[z].min()]).max() / 2.0
    mean_x = dfCloud[x].mean()
    mean_y = dfCloud[y].mean()
    mean_z = dfCloud[z].mean()
    xrange = (mean_x - max_range, mean_x + max_range)
    yrange = (mean_y - max_range, mean_y + max_range)
    zrange = (mean_z - max_range, mean_z + max_range)

    dfCloud['PointSize'] = np.ones(len(dfCloud['distance'])) * 0.1

    if not 'symbol' in dfCloud:
        dfCloud['symbol'] = 0

    fig = px.scatter_3d(dfCloud,x=x,y=y,z=z,range_x=xrange,range_y=yrange,range_z=zrange,hover_data=hoverData,symbol=symbol,color=color)
    fig.update_layout(go.Layout( scene=dict( aspectmode='cube')),)
    fig.update_traces(marker=dict(size=2))
    if scene_camera:
        fig.update_layout(scene_camera=scene_camera)
    fig.show()

def plotPointCloud2(dfCloud,color=None,scene_camera=None):
    max_range = np.array([dfCloud.x.max() - dfCloud.x.min(), dfCloud.y.max() - dfCloud.y.min(),
                          dfCloud.z.max() - dfCloud.z.min()]).max() / 2.0
    mean_x = dfCloud.x.mean()
    mean_y = dfCloud.y.mean()
    mean_z = dfCloud.z.mean()
    xrange = (mean_x - max_range, mean_x + max_range)
    yrange = (mean_y - max_range, mean_y + max_range)
    zrange = (mean_z - max_range, mean_z + max_range)

    dfCloud['PointSize'] = np.ones(len(dfCloud['distance'])) * 0.1

    if not 'symbol' in dfCloud:
        dfCloud['symbol'] = 0

    fig = go.Figure(data=[go.Scatter3d(
        x=dfCloud.x, y=dfCloud.y, z=dfCloud.z,mode='markers',
        marker=dict(size=2,color=list(map(lambda e: 'rgb('+', '.join(e.astype(str))+')', dfCloud.color)),),)])
    # fig = px.scatter_3d(dfCloud,x='x',y='y',z='z',range_x=xrange,range_y=yrange,range_z=zrange,hover_data=["column", "row","distanceInd"],symbol='symbol',color='intensity')
    # fig.update_layout(go.Layout(scene=dict(aspectmode='cube')),)
    fig.update_scenes(aspectmode="data")
    fig.update_traces(marker=dict(size=1))
    if scene_camera:
        fig.update_layout(scene_camera=scene_camera)
    fig.show()

def plotPixel(df,xys,fieldToPlot='samples',xAxis='distance'):
    fig = go.Figure()
    xBins = df['binnedX'].sort_values().unique()
    if type(xys[0]) != list:
        xys = [xys]
    for x,y in xys:
        x_to_slice_snapped = xBins[np.digitize(x, xBins) - 1]
        slicedDF = df[(df.binnedX == x_to_slice_snapped) & (df.y == y)]
        # colors = cm.jet(np.linspace(0, 1, len(slicedDF)))

        for index, row in slicedDF.iterrows():
            # color = colors[index]
            if xAxis == 'distance':
                fig.add_trace(go.Scatter(x=row['distance'], y=row[fieldToPlot],mode='lines',name=r'$\phi={},\theta={}$'.format(x,y)))
            elif xAxis == 'samples':
                fig.add_trace(go.Scatter(y=row[fieldToPlot],mode='lines',name=r'$\phi={},\theta={}$'.format(x,y)))
    fig.show()

def plotAll(df,fieldToPlot='samples',xAxis='distance'):
    fig = go.Figure()
    for x, y in zip(df['x'],df['y']):
        # colors = cm.jet(np.linspace(0, 1, len(slicedDF)))
        for index, row in df.iterrows():
            # color = colors[index]
            if xAxis == 'distance':
                fig.add_trace(go.Scatter(x=row['distance'], y=row[fieldToPlot], mode='lines'))
            elif xAxis == 'samples':
                fig.add_trace(go.Scatter(y=row[fieldToPlot], mode='lines'))
    fig.show()
